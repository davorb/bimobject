(function() {
  'use strict';

  angular
    .module('app.tasks')
    .controller('TaskOne', TaskOne);

  TaskOne.$inject = ['dataservice', 'logger'];

  function TaskOne(dataservice, logger) {
    var vm = this;
    vm.title = "Problem 1";
    vm.bowerJsonData = { test: "hello" };
    activate();

    vm.firstBackground = 'box-red';
    vm.secondBackground = 'box-blue';

    vm.firstClick = function() {
      vm.firstBackground = 'box-green';
      vm.secondBackground = 'box-red';
    };

    vm.secondClick = function() {
      vm.secondBackground = 'box-green';
      vm.firstBackground = 'box-blue';
    };

    function activate() {
      dataservice.getTaskOne().then(function(data){
        vm.boxes = data;
      });
    }
  }
})();

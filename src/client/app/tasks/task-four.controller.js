(function() {
  'use strict';

  angular
    .module('app.tasks')
    .controller('TaskFour', TaskFour);

  TaskFour.$inject = ['dataservice', 'logger', '$scope'];
  /* @ngInject */
  function TaskFour(dataservice, logger, $scope) {
    var vm = this;
    vm.title = 'Task 4';

    vm.search = function() {
      console.log(`searching for ${$scope.artistName}`);

      return dataservice.getAlbums($scope.artistName)
        .then(function(response){
          vm.albumList = response.data.albums.items;
          console.dir(vm.albumList);
          return vm.albumList;
        }).catch(function(err){
          logger.error('woops');
        });
    };
  }

})();
